odoo.define('web_gantt.GanttView', function (require) {
"use strict";

var ajax = require('web.ajax');
var core = require('web.core');
var data_manager = require('web.data_manager');
var formats = require('web.formats');
var Model = require('web.Model');
var time = require('web.time');
var View = require('web.View');
var form_common = require('web.form_common');
var Dialog = require('web.Dialog');
var session = require('web.session');

var _t = core._t;
var _lt = core._lt;
var QWeb = core.qweb;


var GanttView = require('web_gantt.GanttView')

GanttView.include({
   
    _consolidation_children: function (parent) {
        var self = this;
        var group_bys = self.last_group_bys;
        var children = self._get_all_children(parent.id);

        // First step : create a list of object for the children. The contains (left, consolidation
        // value, consolidation color) where left is position in the bar, and consolidation value is
        // the number to add or remove, and the color is [color, sequence] from the last group_by
        // with these information.
        var leftParent = gantt.getTaskPosition(parent, parent.start_date, parent.end_date).left;
        var getTuple = function(acc, task_id) {
            var task = gantt.getTask(task_id);
            var position = gantt.getTaskPosition(task, task.start_date, task.end_date || task.start_date);
            var left = position.left - leftParent;
            var right = left + position.width;
            var start = {type: "start",
                         task: task,
                         left: left,
                         consolidation: task.consolidation,
                        };
            var stop = {type: "stop",
                        task: task,
                        left: right,
                        consolidation: -(task.consolidation),
                       };
            if (task.consolidation_exclude) {
                start.consolidation_exclude = true;
                start.color = task.consolidation_color;
                stop.consolidation_exclude = true;
                stop.color = task.consolidation_color;
            }
            acc.push(start);
            acc.push(stop);
            return acc;
        };
        var steps = _.reduce(children, getTuple, []);

        // Second step : Order it by "left"
        var orderSteps = _.sortBy(steps, function (el) {
            return el.left;
        });

        // Third step : Create the html for the bar
        // html : the final html code
        var html = "";
        // acc : the amount to display inside the task
        var acc = 0;
        // last_left : the left position of the previous task
        var last_left = 0;
        // exclude : A list of task that are not compatible with the other ones (must be hached)
        var exclude = [];
        // not_exclude : the number of task, that are compatible
        var not_exclude = 0;
        // The ids of the task (exclude and not_exclude)
        var ids = [];
        orderSteps.forEach(function (el) {
            var width = Math.max(el.left - last_left , 0);
            var padding_left = (width === 0) ? 0 : 4;
            if (not_exclude > 0 || exclude.length > 0) {
                var classes = [];
                //content
                var content;
                if (self.type === 'consolidate') {
                    var label = self.fields_view.arch.attrs.string || self.fields[self.fields_view.arch.attrs.consolidation].string;
                    content = parseFloat(acc.toFixed(2)) + "<span class=\"half_opacity\"> " + label + "</span>";
                    if (acc === 0 || width < 15 || (self.consolidation_max && acc === self.consolidation_max)) content = "";
                } else {
                    if (exclude.length + not_exclude > 1) {
                        content = exclude.length + not_exclude;
                    } else {
                        content = el.task.text;
                    }
                }
                //pointer
                var pointer = (exclude.length === 0 && not_exclude === 0) ? "none" : "all";
                // Color 
                if (exclude.length > 0) {
                    classes.push("o_gantt_color" + _.last(exclude) + "_0");
                    if (not_exclude) {
                        classes.push("exclude");
                    }
                } else {
                    var opacity = (self.consolidation_max) ? 5 - Math.floor(10*((acc/(2*self.consolidation_max)))) : 1;
                    if (acc === 0){
                        classes.push("transparent");
                    } else if ((self.consolidation_max) && acc > self.consolidation_max){
                        classes.push("o_gantt_color_red");
                    } else if (self.consolidation_max && parent.create[0] === group_bys[0]) {
                        classes.push("o_gantt_colorgreen_" + opacity);
                    } else if (parent.consolidation_color){
                        classes.push("o_gantt_color" + parent.consolidation_color + "_" + opacity);
                    } else {
                        classes.push("o_gantt_color7_" + opacity);
                    }
                }
                html += "<div class=\"inside_task_bar "+ classes.join(" ") +"\" consolidation_ids=\"" + 
                    ids.join(" ") + "\" style=\"pointer-events: "+pointer+"; padding-left: "+ padding_left + 
                    "px; left:"+(last_left )+"px; width:"+width+"px;\">"+content+"</div>";
            }
            acc = acc + el.consolidation;
            last_left = el.left;
            if(el.type === "start"){
                if (el.consolidation_exclude ) exclude.push(el.task.color);
                else not_exclude++;
                ids.push(el.task.id);
            } else {
                if(el.consolidation_exclude) exclude.pop();
                else not_exclude--;
                ids = _.without(ids, el.task.id);
            }
        });
        return html;
    },
    });

});