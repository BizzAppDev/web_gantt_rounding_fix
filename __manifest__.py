# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Web Gantt rounding fix',
    'category': 'web',
    'description': """
Web Gantt chart view rounding fix.
=============================

""",
    'version': '10.0',    
    'author': 'BizzAppDev',
    'website': 'www.bizzappdev.com',
    'depends': ['web_gantt'],
    'data' : [
        'views/web_gantt_templates.xml',
    ],
    'auto_install': True,
    'license': 'OEEL-1',
}
